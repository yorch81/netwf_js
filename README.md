# netWf 4 JavaScript #

## Description ##
netDb Web Framework JavaScript Client.

## Requirements ##
* [JQuery](https://jquery.com/)
* [Example](https://bitbucket.org/yorch81/netsrv)

## Developer Documentation ##
JsDoc.

## Installation ##
Add Jquery to HTML Document.

~~~

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

~~~

Add netWf.js to HTML Document.

~~~

<script src="/web/js/netWf.js"></script>

~~~

## Example ##
~~~

api = new netWf("12345678987654321"); 

// Get Rows with limit
api.table('test', 10,
    function(response, status){
      if (status == "success"){
        console.log(response);
      }
      else
        console.log("Error in Status");
    });

// Get one row by id
api.tableById('test', 'id', 958,
    function(response, status){
      if (status == "success"){
        console.log(response);
      }
      else
        console.log("Error in Status");
    });

// Get First Row
api.first('test', 'id', '*',
    function(response, status){
      if (status == "success"){
        console.log(response);
      }
      else
        console.log("Error in Status");
    });

// Get Previous Row
api.previous('test', 'id', 10, '*',
    function(response, status){
      if (status == "success")
        console.log(response);
      else
        console.log("Error in Status");
    });

// Get Next Row
api.next('test', 'id', 10, '*',
    function(response, status){
      if (status == "success")
        console.log(response);
      else
        console.log("Error in Status");
    });

// Get Last Row
api.last('test', 'id', '*',
    function(response, status){
      if (status == "success")
        console.log(response);
      else
        console.log("Error in Status");
    });

// Execute Stored Procedure
api.procedure('usp_instest', {'@number':666},
    function(response, status){
      if (status == "success"){
        console.log(response);            
      }
      else
        console.log("Error in Status");
    });

// Execute SQL Script
api.script('script1.sql', {'@number':666},
    function(response, status){
      if (status == "success"){
        console.log(response);            
      }
      else
        console.log("Error in Status");
    });
    
~~~

## References ##
https://es.wikipedia.org/wiki/Representational_State_Transfer

P.D. Let's go play !!!







